package com.goodworklabs.config;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.view.Display;
import android.view.WindowManager;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/*******************************************************************************************************************************************************************************************
 * COPYRIGHTS WEGILE - WEB PROJECT PVT LTD
 * <p>
 * DEVELOPER NAME: JSINGH/SR. ANDROID
 * <p>
 * LOCATION: CHANDIGARH - INDIA
 * <p>
 * DATE: MAR 2015
 * <p>
 * CONFIDENTIALITY NOTICE: This source code file contains confidential information that is legally privileged. Do not copy/re-use/re-distribute this file. If you are not the intended recipient/user of this file, any disclosure, copying, or use of any of the transmitted information is STRICTLY PROHIBITED. If you have received this file in error, please immediately notify this to info@wegile.com and destroy the original transmission/files and its attachments without reading or saving them. Thank you.
 *******************************************************************************************************************************************************************************************/

public class ImageConfig {

    private static int digree=0;

    public static int getImageOrientation(Uri target) throws Exception {

        Matrix matrix = new Matrix();
        ExifInterface exif = new ExifInterface(target.getPath());
        String orientation = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        if (orientation.equals(ExifInterface.ORIENTATION_NORMAL)) {
            digree = 0;
        } else if (orientation.equals(ExifInterface.ORIENTATION_ROTATE_90 + "")) {
            matrix.postRotate(90);
            digree = 90;
        } else if (orientation
                .equals(ExifInterface.ORIENTATION_ROTATE_180 + "")) {
            matrix.postRotate(180);
            digree = 180;
        } else if (orientation
                .equals(ExifInterface.ORIENTATION_ROTATE_270 + "")) {
            matrix.postRotate(270);
            digree = 270;
        }

        return digree;
    }

    public static void rotate(Uri uri, int degrees, Context context) {

        Bitmap b = null;
        InputStream inputStream;

        BitmapFactory.Options options = new BitmapFactory.Options();
        options.inTempStorage = new byte[16 * 1024];
        options.inJustDecodeBounds = true;
        try {
            BitmapFactory
                    .decodeStream(new FileInputStream(uri.getPath()), null, options);
        } catch (FileNotFoundException e2) {
            // TODO Auto-generated catch block
            e2.printStackTrace();
        }
        int IMAGE_MAX_SIZE = 700;
        int scale = 1;
        if (options.outHeight > IMAGE_MAX_SIZE
                || options.outWidth > IMAGE_MAX_SIZE) {
            scale = (int) Math.pow(
                    2,
                    (int) Math.round(Math.log(IMAGE_MAX_SIZE
                            / (double) Math.max(options.outHeight,
                            options.outWidth))
                            / Math.log(0.5)));
        }
        // Decode with inSampleSize
        BitmapFactory.Options o2 = new BitmapFactory.Options();
        o2.inSampleSize = scale;
        o2.inPurgeable = true;
        WindowManager manager = (WindowManager) context
                .getSystemService(Context.WINDOW_SERVICE);
        Display display = manager.getDefaultDisplay();
        o2.outHeight = display.getHeight();
        o2.outWidth = display.getWidth();
        try {
            inputStream = context.getContentResolver().openInputStream(uri);
            b = BitmapFactory.decodeStream(inputStream, null, o2);
            inputStream.close();
        } catch (FileNotFoundException e1) {
            // TODO Auto-generated catch block
            e1.printStackTrace();
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        if (degrees != 0 && b != null) {
            Matrix m = new Matrix();

            m.setRotate(degrees, (float) b.getWidth() / 2,
                    (float) b.getHeight() / 2);
            try {
                Bitmap b2 = Bitmap.createBitmap(b, 0, 0, b.getWidth(),
                        b.getHeight(), m, true);
                if (b != b2) {
                    b.recycle();
                    b = b2;
                }
            } catch (OutOfMemoryError ex) {
                throw ex;
            }
        }
        File file = new File(uri.getPath());
        if (file.exists()) {
            file.delete();
        }
        if (file.exists()) {

        } else {
            try {
                file.createNewFile();
                FileOutputStream stream = new FileOutputStream(file.getPath());
                b.compress(Bitmap.CompressFormat.PNG, 80, stream);
                stream.flush();
                stream.close();
                b.recycle();
            } catch (IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

        }

    }

}