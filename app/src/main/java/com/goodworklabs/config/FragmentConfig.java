package com.goodworklabs.config;

/*******************************************************************************************************************************************************************************************
 * COPYRIGHTS WEGILE - WEB PROJECT PVT LTD
 * <p>
 * DEVELOPER NAME: JSINGH/SR. ANDROID
 * <p>
 * LOCATION: CHANDIGARH - INDIA
 * <p>
 * DATE: MAR 2015
 * <p>
 * CONFIDENTIALITY NOTICE: This source code file contains confidential information that is legally privileged. Do not copy/re-use/re-distribute this file. If you are not the intended recipient/user of this file, any disclosure, copying, or use of any of the transmitted information is STRICTLY PROHIBITED. If you have received this file in error, please immediately notify this to info@wegile.com and destroy the original transmission/files and its attachments without reading or saving them. Thank you.
 *******************************************************************************************************************************************************************************************/
public class FragmentConfig {

    public static final int IMAGE_PREVIEW=1;

    public static final int VIDEO_PREVIEW=2;

}
