package com.goodworklabs.config;

import android.net.Uri;
import android.os.Environment;

import com.goodworklabs.R;
import com.goodworklabs.WGApp;

import java.io.File;

/*******************************************************************************************************************************************************************************************
 * COPYRIGHTS WEGILE - WEB PROJECT PVT LTD
 * <p>
 * DEVELOPER NAME: JSINGH/SR. ANDROID
 * <p>
 * LOCATION: CHANDIGARH - INDIA
 * <p>
 * DATE: MAR 2015
 * <p>
 * CONFIDENTIALITY NOTICE: This source code file contains confidential information that is legally privileged. Do not copy/re-use/re-distribute this file. If you are not the intended recipient/user of this file, any disclosure, copying, or use of any of the transmitted information is STRICTLY PROHIBITED. If you have received this file in error, please immediately notify this to info@wegile.com and destroy the original transmission/files and its attachments without reading or saving them. Thank you.
 *******************************************************************************************************************************************************************************************/
public class PathConfig {

    private static final String appName= WGApp.getInstance().getString(R.string.app_name);
    private static final String SDCARD_DIRECTORY = "/"+appName+"";
    private static final String IMAGE_DIRECTORY = SDCARD_DIRECTORY + "/"+appName+" media";
    private static final String VIDEO_DIRECTORY = SDCARD_DIRECTORY + "/"+appName+" media";

    public static String getVideoPath( String id) {

        String path = Environment.getExternalStorageDirectory()
                + VIDEO_DIRECTORY +"/videos"
                + "/" + id + ".mp4";
        return path;

    }

    public static Uri getVideoUri(String id) {

        File main_directory = new File(
                Environment.getExternalStorageDirectory()
                        + VIDEO_DIRECTORY + "/videos");
        if (!main_directory.exists()) {
            main_directory.mkdirs();

        }

        File file = new File(main_directory.getAbsoluteFile() + "/" + id
                + ".mp4");
        Uri imgUri = Uri.fromFile(file);
        return imgUri;

    }


    public static String getImagePath( String id) {

        String path = Environment.getExternalStorageDirectory()
                + VIDEO_DIRECTORY +"/images"
                + "/" + id + ".jpeg";
        return path;

    }

    public static Uri getImageUri(String id) {

        File main_directory = new File(
                Environment.getExternalStorageDirectory()
                        + VIDEO_DIRECTORY + "/images");
        if (!main_directory.exists()) {
            main_directory.mkdirs();

        }

        File file = new File(main_directory.getAbsoluteFile() + "/" + id
                + ".jpeg");
        Uri imgUri = Uri.fromFile(file);
        return imgUri;

    }

}
