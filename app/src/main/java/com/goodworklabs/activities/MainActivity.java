package com.goodworklabs.activities;
/*******************************************************************************************************************************************************************************************
 * COPYRIGHTS WEGILE - WEB PROJECT PVT LTD
 * <p>
 * DEVELOPER NAME: JSINGH/SR. ANDROID
 * <p>
 * LOCATION: CHANDIGARH - INDIA
 * <p>
 * DATE: MAR 2015
 * <p>
 * CONFIDENTIALITY NOTICE: This source code file contains confidential information that is legally privileged. Do not copy/re-use/re-distribute this file. If you are not the intended recipient/user of this file, any disclosure, copying, or use of any of the transmitted information is STRICTLY PROHIBITED. If you have received this file in error, please immediately notify this to info@wegile.com and destroy the original transmission/files and its attachments without reading or saving them. Thank you.
 *******************************************************************************************************************************************************************************************/
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.CamcorderProfile;
import android.media.ExifInterface;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ToggleButton;

import com.goodworklabs.R;
import com.goodworklabs.config.FragmentConfig;
import com.goodworklabs.config.PathConfig;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.Manifest.permission.READ_EXTERNAL_STORAGE;
import static android.Manifest.permission.RECORD_AUDIO;
import static android.Manifest.permission.WRITE_EXTERNAL_STORAGE;
import static android.Manifest.permission.CAMERA;


public class MainActivity extends BaseActivity implements View.OnTouchListener , View.OnClickListener,SurfaceHolder.Callback, SensorEventListener {

    private static final int REQUEST_EXTERNAL_STORAGE_PERMITION=1;

    private ToggleButton            videoOrGallery;
    private ImageView               cameraFlip,redCircle;
    private boolean                 cameraChecked,front,stoping,playing;
    private Uri                     mUri;
    private FloatingActionButton    btnStartRecording;
    private TextView                videoTimer,redText;
    private SurfaceView             videoView;
    private SurfaceHolder           surfaceHolder;
    private Camera                  mCamera;
    private MediaRecorder           mRec = new MediaRecorder();
    private String                  TAG=this.getClass().getName();
    private long                            sec;
    private Handler                 handlerMain;

    private SensorManager           sensorManager;
    private ExifInterface           exif;
    private int                     orientation,degrees;

    public boolean isCameraChecked() {
        return cameraChecked;
    }

    public void setCameraChecked(boolean cameraChecked) {
        this.cameraChecked = cameraChecked;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initialiseView();
        cameraOrVideoSelection();
        getMediaPath();
        // Getting the sensor service.
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        // Register this class as a listener for the accelerometer sensor
        sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_NORMAL);
    }



    private void getMediaPath(){

        if(mayRequestPermition()) {
            mUri = (isCameraChecked()) ? PathConfig.getImageUri("" + System.currentTimeMillis()) : PathConfig.getVideoUri("" + System.currentTimeMillis());
           if(mCamera==null) {
               mCamera = getCameraInstance();

               Camera.Parameters params = mCamera.getParameters();
//            params.setPictureSize(1600, 1200);
               params.setPictureFormat(PixelFormat.JPEG);
               params.setJpegQuality(100);

               params.set("camera-mode",1);
               mCamera.setParameters(params);

               mCamera.setDisplayOrientation(90);

               Handler visible = new Handler();
               visible.postDelayed(new Runnable() {

                   @Override
                   public void run() {

                       try {

                           mCamera.setPreviewDisplay(surfaceHolder);
                           mCamera.startPreview();
                       } catch (IOException e) {
                           e.printStackTrace();
                       }

                       btnStartRecording.setEnabled(true);
                   }
               }, 1000);
           }
        }

    }


    private boolean mayRequestPermition() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            return true;
        }
        if (checkSelfPermission(WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(CAMERA) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED && checkSelfPermission(RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED ) {
            return true;
        }
        if (shouldShowRequestPermissionRationale(WRITE_EXTERNAL_STORAGE)) {
            Snackbar.make(videoOrGallery, R.string.permission_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(android.R.string.ok, new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M)
                            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,READ_EXTERNAL_STORAGE,CAMERA,RECORD_AUDIO}, REQUEST_EXTERNAL_STORAGE_PERMITION);
                        }
                    });
        } else {
            requestPermissions(new String[]{WRITE_EXTERNAL_STORAGE,READ_EXTERNAL_STORAGE,CAMERA,RECORD_AUDIO},REQUEST_EXTERNAL_STORAGE_PERMITION);
        }
        return false;
    }

    @Override
    public void onClick(View v) {
        Log.v(TAG,"onClick");

        mCamera.takePicture(null, null, mPicture);
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        Log.v(TAG,"onTouch");
        int action = event.getAction();

        switch (action){
            case MotionEvent.ACTION_DOWN:
                startVideoRecording();
                btnStartRecording.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.shadowColor)));
                break;

            case MotionEvent.ACTION_UP:
                if(!stoping && playing) {
                    stoping=true;
                    btnStartRecording.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
                    stopVideoRecording();
                }else if(!playing){
                    stoping=true;
                    btnStartRecording.setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
                    mCamera.stopPreview();
                    mCamera.lock();
                    mCamera.release();
                    mCamera = null;
                    getMediaPath();
                }
                break;
        }



        return true;
    }


    private void initialiseView(){
        redCircle = (ImageView) findViewById(R.id.redCircle);
        btnStartRecording = (FloatingActionButton) findViewById(R.id.recordingButton);
        btnStartRecording.setOnTouchListener(this);
        btnStartRecording.setEnabled(false);
        videoOrGallery=(ToggleButton)findViewById(R.id.videoOrGallery);
        cameraFlip=(ImageView)findViewById(R.id.cameraFlip);
        videoTimer = (TextView) findViewById(R.id.videoTimer);
        redText=(TextView)findViewById(R.id.redText);
        videoView = (SurfaceView) findViewById(R.id.videoView);
        surfaceHolder = videoView.getHolder();
        surfaceHolder.addCallback(this);
        btnStartRecording.setOnTouchListener(MainActivity.this);
    }


    private void cameraOrVideoSelection(){
        videoOrGallery.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                setCameraChecked(isChecked);
                File file=new File(mUri.getPath());
                file.delete();
                if(isChecked) {
                    cameraFlip.setImageDrawable(getResources().getDrawable(R.drawable.ic_switch_camera));
                    btnStartRecording.setOnClickListener(MainActivity.this);
                    btnStartRecording.setOnTouchListener(null);
                    redCircle.setVisibility(View.GONE);
                    videoTimer.setVisibility(View.GONE);
                    redText.setVisibility(View.GONE);
                    getMediaPath();
                }
                else {
                    cameraFlip.setImageDrawable(getResources().getDrawable(R.drawable.ic_switch_video));
                    btnStartRecording.setOnTouchListener(MainActivity.this);
                    redCircle.setVisibility(View.VISIBLE);
                    videoTimer.setVisibility(View.VISIBLE);
                    redText.setVisibility(View.VISIBLE);
                    getMediaPath();
                }
            }
        });

    }


    /**
     * Callback received when a permissions request has been completed.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if(requestCode==REQUEST_EXTERNAL_STORAGE_PERMITION){
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                getMediaPath();
            }
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder holder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder holder, int format, int width, int height) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder holder) {
        releaseMediaRecorder();
        releaseCamera();
    }



    public void onCameraFilp(View view) {

        if(!front){
            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;
            Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
            Log.d("No of cameras",Camera.getNumberOfCameras()+"");
            for (int camNo = 0; camNo < Camera.getNumberOfCameras(); camNo++) {
                Camera.CameraInfo camInfo = new Camera.CameraInfo();
                Camera.getCameraInfo(camNo, camInfo);

                if (camInfo.facing==(Camera.CameraInfo.CAMERA_FACING_FRONT)) {
                    mCamera = Camera.open(camNo);
                    mCamera.setDisplayOrientation(90);
                }
            }
            if (mCamera == null) {
                // no front-facing camera, use the first back-facing camera instead.
                // you may instead wish to inform the user of an error here...
                mCamera = Camera.open();
            }

            Handler visible=new Handler();
            visible.postDelayed(new  Runnable() {

                @Override
                public void run() {

                    try {

                        mCamera.setPreviewDisplay(surfaceHolder);
                        mCamera.startPreview();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btnStartRecording.setEnabled(true);
                }
            }, 1000);

            front=true;

        }else{

            mCamera.stopPreview();
            mCamera.release();
            mCamera = null;

            mCamera = Camera.open();
            mCamera.setDisplayOrientation(90);


            Handler visible=new Handler();
            visible.postDelayed(new  Runnable() {

                @Override
                public void run() {

                    try {

                        mCamera.setPreviewDisplay(surfaceHolder);
                        mCamera.startPreview();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    btnStartRecording.setEnabled(true);
                }
            }, 1000);
        }
    }



    private void releaseCamera(){
        if(mCamera!=null) {
            mCamera.release();
            mCamera = null;
        }
    }

    private void releaseMediaRecorder() {
        if (mRec != null) {
            mRec.reset(); // clear recorder configuration
            mRec.release(); // release the recorder object
            mRec = null;
            mCamera.lock(); // lock camera for later use
        }
    }


    private void startVideoRecording(){
        if(!playing){
            mCamera.stopPreview();
            mCamera.lock();
            mCamera.release();
            mCamera = Camera.open();
            mCamera.setDisplayOrientation(90);
            mRec = new MediaRecorder(); // Works well
            mCamera.unlock();
            mRec.setCamera(mCamera);
            mRec.setPreviewDisplay(surfaceHolder.getSurface());

            mRec.setVideoSource(MediaRecorder.VideoSource.CAMERA);
            mRec.setAudioSource(MediaRecorder.AudioSource.MIC);
            // mRec.setOutputFormat(MediaRecorder.OutputFormat.DEFAULT);
            // mRec.setVideoEncoder(MediaRecorder.VideoEncoder.H264);
            mRec.setProfile(CamcorderProfile.get(CamcorderProfile.QUALITY_HIGH));
            mRec.setPreviewDisplay(surfaceHolder.getSurface());


            mRec.setOutputFile(mUri.getPath());
            // mediaRecorder.setMaxDuration(60000); // Set max duration 60 sec.
            mRec.setOrientationHint(90);
//			mRec.setMaxFileSize(size);

            mRec.setOnInfoListener(new MediaRecorder.OnInfoListener() {

                @Override
                public void onInfo(MediaRecorder arg0, int arg1, int arg2) {
                    if (arg1 == MediaRecorder.MEDIA_RECORDER_INFO_MAX_FILESIZE_REACHED) {

                      

                        if (mRec != null) {
                            mRec.stop();
                            mRec.release();
                            mRec = null;
                            handlerMain.removeCallbacks(runnable);

                        }

                       

                    }
                }
            });


            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {

                @Override
                public void run() {
                    if(!stoping) {
                        try {
                            // btnStartRecording.setVisibility(View.INVISIBLE);
                            // btnStopRecording.setVisibility(View.VISIBLE);
                            startRecording();
                            handlerMain = new Handler();
                            handlerMain.post(runnable);
                            playing = true;
                        } catch (Exception e) {
                            e.printStackTrace();
                            String message = e.getMessage();
                            Log.i(null, "Problem Start" + message);
                            if (mRec != null)
                                mRec.release();
                            playing = false;
                            cameraFlip.setImageDrawable(getResources().getDrawable(R.drawable.ic_switch_camera));
                        }
                    }else
                        stoping=false;
                }
            }, 1000);


        }
    }

    protected void startRecording() throws IOException {
        try {
            mRec.prepare();
            mRec.start();
        }catch (Exception e){
            e.printStackTrace();
            playing=false;
            cameraFlip.setImageDrawable(getResources().getDrawable(R.drawable.ic_switch_camera));
        }
    }

    protected void stopRecording() {
        mRec.stop();
        mRec.release();
        mCamera.release();
    }

    private void stopVideoRecording(){

        if (mRec != null) {
            mRec.stop();
            mRec.release();
            mRec = null;
            handlerMain.removeCallbacks(runnable);
			/*
			 * try{ final MediaPlayer mp1=MediaPlayer.create(getBaseContext(),
			 * R.raw.camera_focusing_01); mp1.start(); }catch (Exception e) { //
			 */
        }

        releaseCamera();

        Intent intent=new Intent(MainActivity.this,FragmentBaseActivity.class);
        intent.putExtra("type", FragmentConfig.VIDEO_PREVIEW);
        intent.putExtra("uri",mUri.getPath());
        startActivity(intent);
        finish();
    }



    Runnable runnable = new Runnable() {

        @Override
        public void run() {
			/*
			 * try{ final MediaPlayer mp1=MediaPlayer.create(getBaseContext(),
			 * R.raw.camera_focusing_01); mp1.start(); }catch (Exception e) { //
			 */
            sec = ++sec;
            if (sec % 2 == 0) {
                redCircle.setVisibility(View.VISIBLE);
            } else {
                redCircle.setVisibility(View.INVISIBLE);
            }
            int s = (int) ((sec) % 60);
            int m = (int) ((sec / 60) % 60);
            String sec = "";
            String min = "";
            if (s < 10) {
                sec = "0" + s;
            } else {
                sec = "" + s;
            }
            if (m < 10) {
                min = "0" + m;
            } else {
                min = "" + m;
            }
            videoTimer.setText(min + ":" + sec);
            handlerMain.postDelayed(runnable, 1000);
        }
    };



    private Camera.PictureCallback mPicture = new Camera.PictureCallback() {

        public void onPictureTaken(byte[] data, Camera camera) {

            // Main file where to save the data that we recive from the camera
            File pictureFile = new File(mUri.getPath());

            try {
                FileOutputStream purge = new FileOutputStream(pictureFile);
                purge.write(data);
                purge.close();
            } catch (FileNotFoundException e) {
                Log.d("DG_DEBUG", "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.d("DG_DEBUG", "Error accessing file: " + e.getMessage());
            }

            // Adding Exif data for the orientation. For some strange reason the
            // ExifInterface class takes a string instead of a file.
            try {
                exif = new ExifInterface(mUri.getPath());
                exif.setAttribute(ExifInterface.TAG_ORIENTATION, "" + orientation);
                exif.saveAttributes();
            } catch (IOException e) {
                e.printStackTrace();
            }
            Intent intent=new Intent(MainActivity.this,FragmentBaseActivity.class);
            intent.putExtra("type", FragmentConfig.IMAGE_PREVIEW);
            intent.putExtra("uri",mUri.getPath());
            startActivity(intent);
            finish();
        }
    };



    /**
     * Putting in place a listener so we can get the sensor data only when
     * something changes.
     */
    public void onSensorChanged(SensorEvent event) {
        synchronized (this) {

            if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                RotateAnimation animation = null;
                if (event.values[0] < 4 && event.values[0] > -4) {
                    if (event.values[1] > 0 && orientation != ExifInterface.ORIENTATION_ROTATE_90) {
                        // UP
                        orientation = (front)?2:ExifInterface.ORIENTATION_ROTATE_90;

                        degrees = 270;
                    } else if (event.values[1] < 0 && orientation != ExifInterface.ORIENTATION_ROTATE_270) {
                        // UP SIDE DOWN
                        orientation = (front)?ExifInterface.ORIENTATION_ROTATE_90:ExifInterface.ORIENTATION_ROTATE_270;

                        degrees = 90;
                    }
                } else if (event.values[1] < 4 && event.values[1] > -4) {
                    if (event.values[0] > 0 && orientation != ExifInterface.ORIENTATION_NORMAL) {
                        // LEFT
                        orientation = (front)?ExifInterface.ORIENTATION_ROTATE_180:ExifInterface.ORIENTATION_NORMAL;

                        degrees = 0;
                    } else if (event.values[0] < 0 && orientation != ExifInterface.ORIENTATION_ROTATE_180) {
                        // RIGHT
                        orientation = (front)?ExifInterface.ORIENTATION_NORMAL:ExifInterface.ORIENTATION_ROTATE_180;

                        degrees = 180;
                    }
                }



            }

        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    /**
     * A safe way to get an instance of the Camera object.
     */
    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            // attempt to get a Camera instance
            c = Camera.open();
        } catch (Exception e) {
            // Camera is not available (in use or does not exist)
            e.printStackTrace();
        }

        // returns null if camera is unavailable
        return c;
    }
}
