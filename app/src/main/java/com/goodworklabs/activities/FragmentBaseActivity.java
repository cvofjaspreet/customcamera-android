package com.goodworklabs.activities;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.View;
import android.widget.Toolbar;

import com.goodworklabs.R;
import com.goodworklabs.config.FragmentConfig;
import com.goodworklabs.fragments.ImagePreviewFragment;
import com.goodworklabs.fragments.VideoPreviewFragment;

/*******************************************************************************************************************************************************************************************
 * COPYRIGHTS WEGILE - WEB PROJECT PVT LTD
 * <p>
 * DEVELOPER NAME: JSINGH/SR. ANDROID
 * <p>
 * LOCATION: CHANDIGARH - INDIA
 * <p>
 * DATE: MAR 2015
 * <p>
 * CONFIDENTIALITY NOTICE: This source code file contains confidential information that is legally privileged. Do not copy/re-use/re-distribute this file. If you are not the intended recipient/user of this file, any disclosure, copying, or use of any of the transmitted information is STRICTLY PROHIBITED. If you have received this file in error, please immediately notify this to info@wegile.com and destroy the original transmission/files and its attachments without reading or saving them. Thank you.
 *******************************************************************************************************************************************************************************************/
public class FragmentBaseActivity extends BaseActivity {

    private String  TAG=this.getClass().getName();
    private android.support.v7.widget.Toolbar mToolbar;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment_base);
        TAG = getClass().getName();
        mToolbar=(android.support.v7.widget.Toolbar) findViewById(R.id.toolbarF);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        int type = getIntent().getIntExtra("type",-1);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        switch (type) {

            case FragmentConfig.IMAGE_PREVIEW:
                mToolbar.setTitle("Image Preview");
                Fragment imagePreviewFragment = new ImagePreviewFragment();
                imagePreviewFragment.setArguments(getIntent().getExtras());
                ft.add(R.id.container, imagePreviewFragment, "imagePreviewFragment");
                ft.commit();
                break;


            case FragmentConfig.VIDEO_PREVIEW:

                    mToolbar.setTitle("Video Preview");
                Fragment videoPreviewFragment = new VideoPreviewFragment();
                videoPreviewFragment.setArguments(getIntent().getExtras());
                ft.add(R.id.container, videoPreviewFragment, "videoPreviewFragment");
                ft.commit();
                break;
        }
    }
}
