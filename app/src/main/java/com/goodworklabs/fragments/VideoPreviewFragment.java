package com.goodworklabs.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.goodworklabs.R;

/*******************************************************************************************************************************************************************************************
 * COPYRIGHTS WEGILE - WEB PROJECT PVT LTD
 * <p>
 * DEVELOPER NAME: JSINGH/SR. ANDROID
 * <p>
 * LOCATION: CHANDIGARH - INDIA
 * <p>
 * DATE: MAR 2015
 * <p>
 * CONFIDENTIALITY NOTICE: This source code file contains confidential information that is legally privileged. Do not copy/re-use/re-distribute this file. If you are not the intended recipient/user of this file, any disclosure, copying, or use of any of the transmitted information is STRICTLY PROHIBITED. If you have received this file in error, please immediately notify this to info@wegile.com and destroy the original transmission/files and its attachments without reading or saving them. Thank you.
 *******************************************************************************************************************************************************************************************/
public class VideoPreviewFragment extends Fragment {

    private ImageView videoThumb;
    private String    mUri;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_video_preview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        videoThumb=(ImageView)view.findViewById(R.id.videoThumb);
        mUri=getArguments().getString("uri");
        Bitmap thumbnail = ThumbnailUtils.createVideoThumbnail(mUri,
                MediaStore.Images.Thumbnails.MINI_KIND);
        videoThumb.setImageBitmap(thumbnail);


        videoThumb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.valueOf(mUri)));
                intent.setDataAndType(Uri.parse(String.valueOf(mUri)), "video/*");
                startActivity(intent);
            }
        });
    }
}
