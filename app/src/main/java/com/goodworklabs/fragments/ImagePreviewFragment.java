package com.goodworklabs.fragments;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.goodworklabs.R;
import com.goodworklabs.config.ImageConfig;

import java.io.File;

/*******************************************************************************************************************************************************************************************
 * COPYRIGHTS WEGILE - WEB PROJECT PVT LTD
 * <p>
 * DEVELOPER NAME: JSINGH/SR. ANDROID
 * <p>
 * LOCATION: CHANDIGARH - INDIA
 * <p>
 * DATE: MAR 2015
 * <p>
 * CONFIDENTIALITY NOTICE: This source code file contains confidential information that is legally privileged. Do not copy/re-use/re-distribute this file. If you are not the intended recipient/user of this file, any disclosure, copying, or use of any of the transmitted information is STRICTLY PROHIBITED. If you have received this file in error, please immediately notify this to info@wegile.com and destroy the original transmission/files and its attachments without reading or saving them. Thank you.
 *******************************************************************************************************************************************************************************************/
public class ImagePreviewFragment extends Fragment {
    private ImageView imagePreview;
    private String    mUri;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_image_preview, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        imagePreview=(ImageView)view.findViewById(R.id.imagePreview);
        mUri=getArguments().getString("uri");
        Uri uri=Uri.fromFile(new File(mUri));
        try {
            int rotation=ImageConfig.getImageOrientation(uri);
            if(rotation!=0)
                ImageConfig.rotate(uri,rotation,getContext());
            imagePreview.setImageURI(uri);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
