package com.goodworklabs;

import android.app.Application;

/*******************************************************************************************************************************************************************************************
 * COPYRIGHTS WEGILE - WEB PROJECT PVT LTD
 * <p>
 * DEVELOPER NAME: JSINGH/SR. ANDROID
 * <p>
 * LOCATION: CHANDIGARH - INDIA
 * <p>
 * DATE: MAR 2015
 * <p>
 * CONFIDENTIALITY NOTICE: This source code file contains confidential information that is legally privileged. Do not copy/re-use/re-distribute this file. If you are not the intended recipient/user of this file, any disclosure, copying, or use of any of the transmitted information is STRICTLY PROHIBITED. If you have received this file in error, please immediately notify this to info@wegile.com and destroy the original transmission/files and its attachments without reading or saving them. Thank you.
 *******************************************************************************************************************************************************************************************/
public class WGApp extends Application {


    public static WGApp APP;

    public WGApp() {

        APP = this;
    }


    public static WGApp getInstance() {

        if (APP == null) {
            throw new IllegalStateException();
        }

        return APP;
    }
}
